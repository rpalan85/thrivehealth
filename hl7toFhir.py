#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Converts HL7 messages to FHIR and POST to validation url. 
2020-12-06

"""
__version__ = "1.0.0"

import hl7
import requests
import json
from datetime import datetime

def get_request(url):
    try:
        response = requests.get(url)
        return response
    except Exception as err:
        print('GET request failed: {0}'.format(err))
        return None

def post_request(url, data):
    try:
        data_json = json.dumps(data)
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.post(url, data=data_json, headers=headers)
        return response
    except Exception as err:
        print('POST request failed: {0}'.format(err))
        return None

def hl7_messages_to_fhir(messages):
    try:
        fhir_results = []
        if messages != None and len(messages) > 0:
            for message in messages:
                if message:
                    parsedMessage = hl7.parse(message)
                    if parsedMessage:
                        if is_valid_message(parsedMessage, "94309-2", ["DETECTED", "POSITIVE"], ["NOT"]):
                            fhir_data = hl7_to_fhir(parsedMessage)
                            if fhir_data != None:
                                fhir_results.append(fhir_data)
        return fhir_results
    except Exception as err:
        print('Could not transform HL7 messages to FHIR: {0}'.format(err))
        return None
                        
                        


def is_valid_message(message, loinc_code, include, not_include):
    try:
        obx_segments = message["OBX"]
        for obx_segment in obx_segments:
            if obx_segment[3][0][0][0] == loinc_code:
                findings = obx_segment[5][0]
                if any(x in findings for x in include) and not any(x in findings for x in not_include):
                    return True
    except Exception as err:
        print('Could not validate HL7 message: {0}'.format(err))
    return False
        

def hl7_to_fhir(message):
    try:
        fhir_json = {
            "resourceType": "Patient",
            "id": message["PID"][0][2][0],
            "identifier": [
                {
                    "use": "usual",
                    "label": "MRN",
                    "system": "urn:oid:1.2.36.146.595.217.0.1",
                    "value": message["PID"][0][2][0],
                    "period": {
                        "start": "2020-12-06"
                    },
                    "assigner": {
                        "display": message["MSH"][0][6][0]
                    }
                }
            ],
            "name": [
                {
                    "family": message["PID"][0][5][0][0][0],
                    "given": [
                        message["PID"][0][5][0][1][0]
                    ]
                }
            ],
            "telecom": [
                {
                    "system": "phone",
                    "value": message["PID"][0][13][0],
                    "use": "home"
                }
            ],
            "gender": {
                "coding": [
                    {
                        "system": "http://hl7.org/fhir/v3/AdministrativeGender",
                        "code": message["PID"][0][8][0],
                        "display": "Male" if message["PID"][0][8][0] == "M" else "Female"
                    }
                ]
            },
            "birthDate": datetime.strftime(datetime.strptime(message["PID"][0][7][0], "%Y%m%d"), "%Y-%m-%d")
        }
        return fhir_json
    except Exception as err:
        print('Could not transform hl7 to fhir: {0}'.format(err))
        return None


response = get_request("https://challenge.thrive.dev/one")
if response != None and response.text:
    hl7_messages = response.text.split("\r\r")        
    fhir_data = hl7_messages_to_fhir(hl7_messages)
    if fhir_data != None:
        response = post_request("https://challenge.thrive.dev/one", fhir_data)
        print(response)
